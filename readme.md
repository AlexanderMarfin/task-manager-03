# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Alexander Marfin

* **E-Mail**: amarfin@t1-consulting.ru

## SOFTWARE

* OpenJDK 8

* Intelliji Idea

* MS Windows 7 x64

## HARDWARE

* **RAM**: 8gb

* **CPU**: Intel Core i3

* **HDD**: 512Gb

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```